import { Application } from '../declarations';
import balances from './balances/balances.service';
import deposits from './deposits/deposits.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application): void {
  app.configure(balances);
  app.configure(deposits);
}
