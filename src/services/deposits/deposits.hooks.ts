
import validateDeposit from '../../hooks/validate-deposit';
import updateBalance from '../../hooks/update-balance';
import updateBalanceCheat from '../../hooks/update-balance-cheat';
export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [validateDeposit()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
//        updateBalance(),
        updateBalanceCheat()
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
